#include "ctictactoe.h"


// start ...
void CTicTacToe::start()
{
    while(true)
    {
        std::cout << "Do you want to play? [y]es, [n]o ";
        char c;
        std::cin >> c;
        if(c != 'y') break;

        std::cout << "Is the first player a [h]uman or a [c]omputer? ";
        std::cin >> c;
        player1 = ((c != 'c')?(&CTicTacToe::receiveSymbolFromHuman):(&CTicTacToe::receiveSymbolFromComputer));

        std::cout << "Is the second player a [h]uman or a [c]omputer? ";
        std::cin >> c;
        player2 = ((c != 'c')?(&CTicTacToe::receiveSymbolFromHuman):(&CTicTacToe::receiveSymbolFromComputer));


        GameParams::getGame();
        Field::printField();

        while(true)
        {
            (GameParams::getNumberStep() % 2)
                ?(this->*player2)()
                :(this->*player1)();
            Field::printField();
            _statusGame = checkWin();

            if(_statusGame == During)
                GameParams::setCurrentSymbol((GameParams::getCurrentSymbol() == GameParams::Cross)
                                         ?GameParams::Nought
                                         :GameParams::Cross);
            else
                break;
        }

        switch(_statusGame)
        {
            case Win:
                std::cout << ((GameParams::getCurrentSymbol() == GameParams::Cross)?"Cross":"Nought")
                          << " won!" << std::endl;
                break;

            case Draw:
                std::cout << "Good, we have a draw!" << std::endl;
                break;

            case During:
                break;
        }

        endGame();
    }
}


// endGame ...
void CTicTacToe::endGame()
{
    GameParams::clear();
    _statusGame = During;
}


// receiveSymbolFromHuman ...
void CTicTacToe::receiveSymbolFromHuman()
{
    Coord point;

    do
    {
        std::cout << "Enter number row and column: ";
        std::cin >> point.x >> point.y;
        point.x--;
        point.y--;
    }
    while(checkPosition(point));

    GameParams::setElement(point.x, point.y, GameParams::getCurrentSymbol());

    lastPoint.x = point.x;
    lastPoint.y = point.y;
}


// receiveSymbolFromHuman ...
void CTicTacToe::receiveSymbolFromComputer()
{
    Coord point;

    std::srand(std::time(nullptr));

    do
    {
        int counter = 0;
        int r = 1 + std::rand() % (GameParams::getSizeField() * GameParams::getSizeField() - GameParams::getNumberStep());

        for(int i = 0; i < GameParams::getSizeField(); ++i)
        {
            for(int j = 0; j < GameParams::getSizeField(); ++j)
            {
                if(GameParams::getElement(i, j) == GameParams::Empty)
                {
                    ++counter;

                    if(counter == r)
                    {
                        point.x = i;
                        point.y = j;
                        break;
                    }
                }
            }

            if(counter == r) break;
        }
    }
    while(checkPosition(point));

    GameParams::setElement(point.x, point.y, GameParams::getCurrentSymbol());

    lastPoint.x = point.x;
    lastPoint.y = point.y;
}


// checkPosition ...
bool CTicTacToe::checkPosition(Coord &p)
{
    return (GameParams::getElement(p.x, p.y) != GameParams::Empty);
}


// checkWin ...
CTicTacToe::StatusGame CTicTacToe::checkWin()
{
    // Проверка номера хода (если меньше 2n - 1 точно в процессе, где n кол-во клеток для победы)
    if(GameParams::getNumberStep() < (2 * GameParams::getCountCellToWin() - 1))
        return During;

    // Проверка на победу

    // Диагональ слева направо (вверх)
    Coord p = lastPoint;
    int counterSymbol = 0;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.x >= 0 && p.y >= 0)
    {
        p.x--;
        p.y--;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Диагональ слева направо (вниз)
    p.x = lastPoint.x + 1;
    p.y = lastPoint.y + 1;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.x < GameParams::getSizeField() && p.y < GameParams::getSizeField())
    {
        p.x++;
        p.y++;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Диагональ справа налево (вверх)
    p = lastPoint;
    counterSymbol = 0;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.x >= 0 && p.y < GameParams::getSizeField())
    {
        p.x--;
        p.y++;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Диагональ справа налево (вниз)
    p.x = lastPoint.x + 1;
    p.y = lastPoint.y - 1;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.x < GameParams::getSizeField() && p.y <= 0)
    {
        p.x++;
        p.y--;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Горизонталь (влево)
    p = lastPoint;
    counterSymbol = 0;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.y >= 0)
    {
        p.y--;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Горизонталь (вправо)
    p.y = lastPoint.y + 1;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.y < GameParams::getSizeField())
    {
        p.y++;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Вертикаль (ввверх)
    p = lastPoint;
    counterSymbol = 0;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.x >= 0)
    {
        p.x--;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Вертикаль (вниз)
    p.x = lastPoint.x + 1;

    while(GameParams::getElement(p.x, p.y) == GameParams::getCurrentSymbol() &&
          p.x < GameParams::getSizeField())
    {
        p.x++;
        ++counterSymbol;

        if(counterSymbol == GameParams::getCountCellToWin()) return Win;
    }

    // Проверка на ничью (кол-во ходов равно кол-ву клеток)
    if(GameParams::getNumberStep() == GameParams::getSizeField() * GameParams::getSizeField())
        return Draw;

    return During;
}
