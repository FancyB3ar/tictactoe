#ifndef CTICTACTOE_H
#define CTICTACTOE_H

#include <iostream>
#include <ctime>

#include "tools/gameparams/gameparams.h"
#include "tools/field/field.h"


struct CTicTacToe
{
private:

    struct Coord
    {
        int x;
        int y;
    };

    enum StatusGame
    {
        During,
        Win,
        Draw
    };

    StatusGame _statusGame = During;
    Coord lastPoint;

    void (CTicTacToe::*player1)();                              // Уазатель на функцию первого игрока
    void (CTicTacToe::*player2)();                              // Уазатель на функцию второго игрока

    void receiveSymbolFromHuman();                      // Функция получения символа от человека
    void receiveSymbolFromComputer();                   // Функция получения символа от компьютера
    bool checkPosition(Coord &p);                       // Функция проверки позиции для постановки символа
    StatusGame checkWin();                              // Функция проверки на победу
    void endGame();                                     // Функция окончания игры

public:
    void start();                                       // Функция начала игры
};

#endif // CTICTACTOE_H
