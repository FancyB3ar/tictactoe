#include "field.h"


void Field::printField()
{
    std::cout << " ";

    for(int i = 0; i < GameParams::getSizeField(); ++i)
        std::cout << " " << (i + 1);

    printLine();

    for(int i = 0; i < GameParams::getSizeField(); ++i)
    {
        std::cout << (i + 1) << "|";

        for(int j = 0; j < GameParams::getSizeField(); ++j)
        {
            switch(GameParams::getElement(i, j))
            {
                case GameParams::Empty:
                    std::cout << " ";
                    break;

                case GameParams::Cross:
                    std::cout << "X";
                    break;

                case GameParams::Nought:
                    std::cout << "O";
                    break;

                case GameParams::Error:
                    std::cout << "@";
                    break;
            }

            std::cout << "|";
        }

        printLine();
    }
}


void Field::printLine()
{
    std::cout << std::endl << " ";

    for(int i = 0; i < GameParams::getSizeField() * 2 + 1; ++i)
        std::cout << "-";

    std::cout << std::endl;
}
