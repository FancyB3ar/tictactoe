#ifndef FIELD_H
#define FIELD_H

#include <iostream>

#include "tools/gameparams/gameparams.h"


struct Field
{
private:
    static void printLine();

public:
    static void printField();      // Функция отрисовки поля
};

#endif // FIELD_H
