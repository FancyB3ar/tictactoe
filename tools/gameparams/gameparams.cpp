#include "gameparams.h"


int GameParams::_sizeField = -1;
int GameParams::_countCellToWin = -1;
GameParams::Symbol **GameParams::arrGame = nullptr;
GameParams::Symbol GameParams::_currentSymbol = Symbol::Cross;
int GameParams::_numberStep = 0;


// getGame ...
void GameParams::getGame()
{
    setSizeField();
    setCountCellToWin();

    allocateMemory();
}


// clear ...
void GameParams::clear()
{
    _sizeField = -1;
    _countCellToWin = -1;
    freeMemory();
    _currentSymbol = Symbol::Cross;
    _numberStep = 0;
}


// setElement ...
void GameParams::setElement(int row, int col, Symbol s)
{
    if(getElement(row, col) == Empty)
    {
        ++_numberStep;
        arrGame[row][col] = s;
    }
}


// getElement ...
GameParams::Symbol GameParams::getElement(int row, int col)
{
    if( (_sizeField < 0) ||
        ((row >= _sizeField) || (row < 0)) ||
        ((col >= _sizeField) || (col < 0)) ) return Error;

    return arrGame[row][col];
}


// setCurrentSymbol ...
void GameParams::setCurrentSymbol(Symbol s)
{
    if(s == Symbol::Cross || s == Symbol::Nought)
        _currentSymbol = s;
}


// getCurrentSymbol ...
GameParams::Symbol GameParams::getCurrentSymbol()
{
    return _currentSymbol;
}


// setSizeField ...
void GameParams::setSizeField()
{
    int countBadTry = 0;

    do
    {
        std::cout << "Enter a field size between " << MIN_SIZE << " and " << MAX_SIZE << ": ";
        std::cin >> _sizeField;

        if(_sizeField < MIN_SIZE || _sizeField > MAX_SIZE) ++countBadTry;
        else break;
    }
    while(countBadTry < MAX_COUNT_TRY_ENTER);
}


// getSizeField ...
int GameParams::getSizeField()
{
    return _sizeField;
}


// setCountCellToWin ...
void GameParams::setCountCellToWin()
{
    if(_sizeField == MIN_SIZE)
    {
        _countCellToWin = _sizeField;
        return;
    }

    int countBadTry = 0;

    do
    {
        std::cout << "Enter count cell to win between " << MIN_SIZE << " and " << _sizeField << ": ";
        std::cin >> _countCellToWin;

        if(_countCellToWin < MIN_SIZE || _countCellToWin > _sizeField) ++countBadTry;
        else break;
    }
    while(countBadTry < MAX_COUNT_TRY_ENTER);
}


// getCountCellToWin ...
int GameParams::getCountCellToWin()
{
    return _countCellToWin;
}


// getNumberStep ...
int GameParams::getNumberStep()
{
    return _numberStep;
}


// allocateMemory ...
void GameParams::allocateMemory()
{
    if(arrGame != nullptr)
    {
        freeMemory();
    }

    arrGame = new Symbol *[_sizeField];

    if(arrGame != nullptr)
    {
        for(int i = 0; i < _sizeField; ++i)
        {
            arrGame[i] = new Symbol [_sizeField];

            if(arrGame[i] == nullptr)
            {
                freeMemory();
                break;
            }
        }
    }
    else
    {
        std::cout << "Error allocate memory" << std::endl;
    }
}


// freeMemory ...
void GameParams::freeMemory()
{
    for(int i = 0; i < _sizeField; ++i)
        delete [] arrGame[i];
    delete [] arrGame;
    arrGame = nullptr;
}
