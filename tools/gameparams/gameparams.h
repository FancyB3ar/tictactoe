#ifndef GAMEPARAMS_H
#define GAMEPARAMS_H

#include <iostream>


struct GameParams
{
    enum Symbol
    {
        Empty,
        Cross,
        Nought,
        Error
    };

private:
    static const int MAX_COUNT_TRY_ENTER = 3;               // Константа с максимальным количеством попыток ввода
    static const int MIN_SIZE = 3;                          // Константа с минимальным значением размера игрового поля
    static const int MAX_SIZE = 5;                          // Константа с максимальным значением размера игрового поля
    static int _sizeField;                                  // Перменная с размером игрового поля
    static int _countCellToWin;                             // Переменная с количеством клеток подряд для выигрыша
    static Symbol _currentSymbol;                           // Переменная с текущим символом игрока
    static int _numberStep;                                 // Переменная счётчик количества шагов игры

    static Symbol **arrGame;                                // Матрица игорвого поля

    static void setSizeField();                             // Функция задания размера игрового поля
    static void setCountCellToWin();                        // Функция задания количества клетокподряд для победы
    static void allocateMemory();                           // Функция выделения памяти для массива игры
    static void freeMemory();                               // Функция освобождения памяти для массива игры

public:
    static void getGame();                                  // Функция получения игры (обязательна перед игрой)
    static void clear();                                    // Функция очистки параметров
    static void setCurrentSymbol(Symbol s);                 // Функция задания текущего символа
    static Symbol getCurrentSymbol();                       // Функция получения текущего символа
    static int getNumberStep();                             // Функция получения номера хода в игре
    static int getSizeField();                              // Функция получения размера игрового поля
    static int getCountCellToWin();                         // Функция получения количества клеток подряд для победы
    static void setElement(int row, int col, Symbol s);     // Функция задания элемента
    static Symbol getElement(int row, int col);             // Функция получения элемента из массива
};

#endif // GAMEPARAMS_H
